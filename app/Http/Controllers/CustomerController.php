<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

class CustomerController extends Controller
{

    public function index(Request $request)
    {

        // Flash user request to persist user input in the view page
        $request->flash();

        // Serach parameters
        $year = $request->txt_birth_year ?? '';
        $month = $request->txt_birth_month ?? '';

        // page number of pagination
        $page = request()->get('page', 1);

        // Item limits per page. Defualt value 20
        $limit = request()->get('limit', 20);

        // Prepare Redis Key
        $key = ($year ? 'Year' . $year : '') . ($month ? 'Month' . $month : '') . $page;

        // Use Redis cache for 60 seconds.
        $customers = Cache::remember($key, 60, function () use ($limit, $year, $month) {
            return Customer::serchCustomers($year, $month, $limit);
        });

        return view('customer.index', compact('customers'));
    }
}
