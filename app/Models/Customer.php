<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;


    public static function serchCustomers($year, $month, $limit)
    {

        $customer = Customer::select('*');
        if ($year && $month) {
            $customer->whereYear('dob', $year)->whereMonth('dob', $month);
        } else if ($year) {
            $customer->whereYear('dob', $year);
        } else if ($month) {
            $customer->whereMonth('dob', $month);
        }

        // appends query string with page URL by functin withQueryString
        return $customer->paginate($limit)->withQueryString();
    }
}
