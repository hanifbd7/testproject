<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Customer List') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="search-form-wrapper">
                <form action="{{route('customer')}}" name="search-form" id="search-form" method="get" class="row g-3" onsubmit="return validateForm()">
                    <div class="col-auto">
                        <label for="birth_year" class="visually-show">Birth Year</label>
                        <input type="number" class="form-control" name="txt_birth_year" placeholder="Ex. 1971" min="1800" max="{{date('Y')}}" value="{{old('txt_birth_year')}}">
                    </div>
                    <div class="col-auto">
                        <label for="birth_month" class="visually-show">Birth Month</label>
                        <input type="number" class="form-control" name="txt_birth_month" placeholder="Ex. 10" min="1" max="12" value="{{old('txt_birth_month')}}">
                    </div>
                    <div class="col-auto" style="padding-top: 22px;">
                        <button type="submit" class="btn btn-primary mb-3">Filter</button>
                    </div>
                </form>
            </div>
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="pagination">
                    {{ $customers->onEachSide(1)->links()}}
                </div>

                <table class="table table-hover">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>DOB</th>
                        <th>Phone</th>
                        <th>Location</th>
                        <th>IP</th>
                    </tr>
                    @forelse ($customers as $key=>$customer)
                    <tr>
                        <td>
                            {{$customer->id}}
                        </td>
                        <td>
                            {{$customer->name}}
                        </td>
                        <td>
                            {{$customer->email}}
                        </td>
                        <td>
                            {{$customer->dob}}
                        </td>
                        <td>
                            {{$customer->phone}}
                        </td>
                        <td>
                            {{$customer->country}}
                        </td>
                        <td>
                            {{$customer->ip}}
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="7">No records found!</td>
                    </tr>
                    @endforelse
                </table>
                <div class="pagination">
                    {{ $customers->onEachSide(1)->links()}}
                </div>
            </div>
        </div>
    </div>
    <script>
        function validateForm() {
            let txt_birth_year = document.forms["search-form"]["txt_birth_year"].value;
            let txt_birth_month = document.forms["search-form"]["txt_birth_month"].value;
            if (txt_birth_year == "" && txt_birth_month == "") {
                return false;
            }
        }
    </script>
</x-app-layout>